import { Component, OnInit } from '@angular/core';
import { config } from './../../config';
import * as AWS from 'aws-sdk/global';
import * as S3 from 'aws-sdk/clients/s3';

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.css']
})
export class SummaryComponent implements OnInit {
  offerData;
  customerData;
  invoiceData;
  productData;

  statusPending = false;
  statusDuplicate = false;
  statusValid = false;

  submissionPending;
  submissionDuplicate;
  submissionSuccess;

  submissionId;
  submissionStatus;

  constructor() {
    try {
      console.log(config);
      if (config.submissionStatus == 'pending') {
        this.statusPending = true;
      }
      else if (config.submissionStatus == 'Duplicate') {
        this.statusDuplicate = true;
      }
      else if (config.submissionStatus == 'valid') {
        this.statusValid = true;
      }
    } catch (error) {
      console.log(error);
    }
  }

  ngOnInit() {
    try {
      this.offerData = JSON.parse(config.entireData.offer);
      this.productData = config.entireData.product;
      this.customerData = JSON.parse(config.entireData.customerInfo);
      if(config.entireData.invoiceImg != 0){
        this.invoiceData = config.entireData.invoiceImg;
      }

      this.submissionId = config.submissionId;
      this.submissionStatus = config.submissionStatus;
      this.submissionPending = config.submissionPending;
      this.submissionDuplicate = config.submissionDuplicate;
      this.submissionSuccess = config.submissionSuccess;
    } catch (error) {
      console.log(error);
    }

  }



}
